import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { PreHome } from '../pages/pre-home/pre-home';
import { Home } from '../pages/home/home';
import { Competicao } from '../pages/competicao/competicao';
import { Noticia } from '../pages/noticia/noticia';
import { Resultado } from '../pages/resultado/resultado';
import { TabelaJogos } from '../pages/tabelajogos/tabelajogos';
import { ConfigPage } from '../pages/config/config';

import { CompeticaoService } from '../providers/competicao-service';
import { Connectivity } from '../providers/connectivity';

import { NativeStorage } from '@ionic-native/native-storage';

/** Filter Notifications Readed Pipe */
import { FilterPipe } from '../components/shared/FilterPipe';

/*IMPORT MODALS*/
import { ModalClassificacao } from '../components/modals/classificacao/ModalClassificacao';
import { ModalArtilheiroDetalhes } from '../components/modals/ModalArtilheiroDetalhes';
import { ModalMelhorDisciplinaDetalhes } from '../components/modals/ModalMelhorDisciplinaDetalhes';
import { ModalAtaquePositivoDetalhes } from '../components/modals/ModalAtaquePositivoDetalhes';
import { ModalMelhorDefesaDetalhes } from '../components/modals/ModalMelhorDefesaDetalhes';
import { ModalGolsContraDetalhes } from '../components/modals/ModalGolsContraDetalhes';
import { ModalGoleiroArtilheiroDetalhes } from '../components/modals/ModalGoleiroArtilheiroDetalhes';
import { ModalCarteira } from '../components/modals/goleiro/ModalCarteira';
import { ModalJogadoresSuspensos } from '../components/modals/simplepage/ModalJogadoresSuspensos';
import { ModalJogadoresPendurados } from '../components/modals/simplepage/ModalJogadoresPendurados';
import { ModalBalancouRede } from '../components/modals/simplepage/ModalBalancouRede';
import { ModalMembro1 } from '../components/modals/simplepage/ModalMembro1';
import { ModalMembro2 } from '../components/modals/simplepage/ModalMembro2';
import { ModalAtestadoEntregue } from '../components/modals/simplepage/ModalAtestadoEntregue';
import { ModalAtestadoPendente } from '../components/modals/simplepage/ModalAtestadoPendente';
import { ModalAniversariantes } from '../components/modals/simplepage/ModalAniversariantes';
import { ModalPautaJulgamento } from '../components/modals/simplepage/ModalPautaJulgamento';
import { ModalDetalhesJogo } from '../components/modals/jogos/ModalDetalhesJogo';
import { ModalJogadorDetalhes } from '../components/modals/simplepage/ModalJogadorDetalhes';
import { ModalNoticia } from '../components/modals/simplepage/ModalNoticia';

import { ModalJogosAdiados } from '../components/modals/simplepage/ModalJogosAdiados';
import { ModalJogosWo } from '../components/modals/simplepage/ModalJogosWo';

import { ModalNotifications } from '../components/modals/ModalNotifications';
import { ModalNotificationView } from '../components/modals/ModalNotificationView';

import { BrowserModule } from '@angular/platform-browser';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { OneSignal } from '@ionic-native/onesignal';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';

import { SQLite } from '@ionic-native/sqlite';

import { DatabaseProvider } from '../providers/database';

@NgModule({
  declarations: [
    MyApp,
    PreHome,
    Home,
    Competicao,
    Noticia,
    Resultado,
    TabelaJogos,
    ConfigPage,
    ModalClassificacao,
    ModalArtilheiroDetalhes,
    ModalMelhorDisciplinaDetalhes,
    ModalAtaquePositivoDetalhes,
    ModalMelhorDefesaDetalhes,
    ModalGolsContraDetalhes,
    ModalGoleiroArtilheiroDetalhes,
    ModalJogadoresSuspensos,
    ModalJogadoresPendurados,
    ModalBalancouRede,
    ModalMembro1,
    ModalMembro2,
    ModalAtestadoEntregue,
    ModalAtestadoPendente,
    ModalAniversariantes,
    ModalPautaJulgamento,
    ModalDetalhesJogo,
    ModalCarteira,
    ModalJogadorDetalhes,
    ModalJogosAdiados,
    ModalJogosWo,
    ModalNoticia,
    ModalNotifications,
    ModalNotificationView,
    FilterPipe
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Home,
    PreHome,
    Competicao,
    Noticia,
    Resultado,
    TabelaJogos,
    ConfigPage,
    ModalClassificacao,
    ModalArtilheiroDetalhes,
    ModalMelhorDisciplinaDetalhes,
    ModalAtaquePositivoDetalhes,
    ModalMelhorDefesaDetalhes,
    ModalGolsContraDetalhes,
    ModalGoleiroArtilheiroDetalhes,
    ModalDetalhesJogo,
    ModalCarteira,
    ModalJogadoresSuspensos,
    ModalJogadoresPendurados,
    ModalBalancouRede,
    ModalMembro1,
    ModalMembro2,
    ModalAtestadoEntregue,
    ModalAtestadoPendente,
    ModalAniversariantes,
    ModalPautaJulgamento,
    ModalJogadorDetalhes,
    ModalJogosAdiados,
    ModalJogosWo,
    ModalNoticia,
    ModalNotifications,
    ModalNotificationView,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CompeticaoService,
    Connectivity,
    {provide: LOCALE_ID, useValue: 'pt-BR'},
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Network,
    NativeStorage,
    OneSignal,
    FileTransfer,
    FileTransferObject,
    SQLite,
    DatabaseProvider
  ]
})
export class AppModule {}
