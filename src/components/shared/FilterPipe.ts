import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'readedfilter',
    pure: false,
})

export class FilterPipe implements PipeTransform {
    transform(items: any[], filter: { readed }): any {
        console.log(items);
        if(!items || !filter){
            return items;
        }
        let arr: any[]=[];
        console.log(filter);
        for(var i = 0; i<items.length;i++){
            if(items[i].readed == filter.readed){
                arr.push(items[i]);
            }
        }
        return arr;
    }
}