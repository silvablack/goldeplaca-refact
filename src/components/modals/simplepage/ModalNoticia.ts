import { Component,ViewChild } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular';
import { Slides } from 'ionic-angular';

@Component({
  templateUrl: './ModalNoticia.html'
})

export class ModalNoticia{
  @ViewChild(Slides) slides: Slides;
  public noticia;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.noticia = params.get('noticia');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }


}
