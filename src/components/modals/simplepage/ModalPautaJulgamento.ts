import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular'
import { CompeticaoService } from '../../../providers/competicao-service';
@Component({
  templateUrl: './ModalPautaJulgamento.html'
})

export class ModalPautaJulgamento{
  public jogadores;
  public competicao;
  public data_analise;
  public horario;
  constructor(public competicaoService: CompeticaoService, public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.jogadores = params.get('jogadores');
    this.data_analise = this.jogadores[0].data_analise;
    this.horario = this.jogadores[0].horario;
    this.nameCompeticao();
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }
  nameCompeticao(){
    this.competicaoService.findOne(this.jogadores[0].id_competicao).subscribe((name)=>{
      console.log(name);
      this.competicao = name[0].nome_competicao;
    });
  }


}
