import { Component, ViewChild } from '@angular/core';
import { Platform, NavParams, NavController, ViewController } from 'ionic-angular';

@Component({
  templateUrl: './ModalJogosAdiados.html',
})

export class ModalJogosAdiados{
  public jogos: Array<any>;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.jogos = params.get('jogos');
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
