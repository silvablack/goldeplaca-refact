import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular'

@Component({
  templateUrl: './ModalArtilheiroDetalhes.html'
})

export class ModalArtilheiroDetalhes{
  public artilheiro;
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.artilheiro = params.get('artilheiro');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }


}
