import { Component, ViewChild } from '@angular/core';
import { Platform, NavParams, NavController, LoadingController, ViewController, AlertController, ModalController } from 'ionic-angular';
import { CompeticaoService } from '../../../providers/competicao-service';
import { ModalCarteira } from '../goleiro/ModalCarteira';
import 'rxjs/add/operator/finally';

@Component({
  templateUrl: './ModalClassificacao.html',
  selector: 'modal-classificacao'
})

export class ModalClassificacao{
  public classificacao:Array<any>;
  public jogadores: Array<any>;
  public jogos: Array<any>;
  public competicao: any;
  constructor(public loadCtrl: LoadingController,public modalCtrl: ModalController,public alertCtrl: AlertController,public platform: Platform,public params: NavParams,public viewCtrl: ViewController,public competicaoService: CompeticaoService){
    console.log(params.get('id'));
    this.mount(params.get('id'));
    let load = loadCtrl.create({content: 'Carregando...'});
    load.present;
    competicaoService.findJogadoresEquipe(params.get('id'))
    .finally(()=>{
      load.dismiss()
    })
    .subscribe((data)=>{
     this.jogadores = data;
    });
    competicaoService.findJogosEquipe(params.get('id'))
    .finally(()=>{
      load.dismiss()
    })
    .subscribe((data)=>{
     this.jogos = data;
    });
  }

  mount(id){
    let load = this.loadCtrl.create({content: 'Carregando...'});
    load.present();
    this.competicaoService.findEquipe(id)
    .finally(()=>{
      load.dismiss()
    })
    .subscribe((response)=>{
      this.classificacao = response[0];
    },(error)=>console.log(error));
  }

  carteiraGoleiro(id){
    this.modalCtrl.create(ModalCarteira,{jogador: id}).present();
  }

  carteiraInfo(){
    this.alertCtrl.create({
      title: "Informação",
      message: "Sua carteira não está disponível, por favor entre em contato com a gerência!",
      buttons: ['Ok']
    })
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
