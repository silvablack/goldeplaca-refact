import { Component } from '@angular/core';
import { Platform, NavParams, ViewController, ModalController } from 'ionic-angular'
import { ModalNotificationView } from './ModalNotificationView';
import { DatabaseProvider } from '../../providers/database';

@Component({
  templateUrl: './ModalNotifications.html'
})

export class ModalNotifications{

  public notifications;

  public read = {readed: 1};
  public notread = {readed: 0};
  
  constructor(private db: DatabaseProvider ,private modal: ModalController, public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
  }

  ionViewWillEnter(){
    this.db.getAll().then((data: any[])=>{
      this.notifications = data;
    }).catch((err)=>{console.log(err)})
  }
  
  dismiss(){
    this.viewCtrl.dismiss();
  }
  
  viewNotification(n){
    this.db.updateRead(n.id, 1).then(()=>{
      this.modal.create( ModalNotificationView,{
        'notification': n
      }).present();
    });
  }



}
