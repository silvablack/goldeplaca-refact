import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular'

@Component({
  templateUrl: './ModalNotificationView.html'
})

export class ModalNotificationView{

  public notification;
  
  constructor(public platform: Platform,public params: NavParams,public viewCtrl: ViewController){
    this.notification = params.get('notification');
  }
  dismiss(){
    this.viewCtrl.dismiss();
  }




}
