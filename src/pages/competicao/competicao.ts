import { Component, ElementRef, ViewChild} from '@angular/core';
import { Events, NavController, Platform, NavParams, Slides, LoadingController, AlertController } from 'ionic-angular';
import { CompeticaoService } from '../../providers/competicao-service';
import { Home } from '../home/home';
import 'rxjs/add/operator/finally';
import { NativeStorage } from '@ionic-native/native-storage';

@Component({
  selector: 'page-competicao',
  templateUrl: 'competicao.html',
  providers: [ CompeticaoService ]
})
export class Competicao {
  @ViewChild('pageElement') pageElement: ElementRef;
  @ViewChild(Slides) slides: Slides;
  selectedItem: any;
  icons: string[];
  load: any;
  items: Array<any> = [];
  banner: Array<any>;

  constructor(
              public events: Events,
              public navCtrl: NavController,
              public navParams: NavParams,
              public competicaoService: CompeticaoService,
              public platform: Platform,
              public loadingCtrl: LoadingController,
              private nativeStorage: NativeStorage,
              private alert: AlertController
            ) {
    // If we navigated to this page, we will have an item available as a nav param
    this.load = this.loadingCtrl.create({content: 'Carregando...'})
    this.load.present();
  }

  ionViewDidLoad(){

      let pageLoaded = this.competicaoService.init(this.pageElement);
      //Consuming WebService
      pageLoaded.then((res)=>{
        console.table({
          'res': res
        });
        //Aparece apenas as competições selecionadas na page PreHome
        this.nativeStorage.getItem('current_competitions')
        .then((competitions)=>{
          for(let x of competitions){
            this.competicaoService.findOne(x.id).finally(()=>{

            }).subscribe((data)=>{
              this.competicaoService.findBanner()
              .finally(()=>{
                if(this.load){ this.load.dismiss(); this.load = null; }
              }).subscribe((data)=>{
                this.banner = data;
              });
              this.items.push(data[0]);
            });
          }

        },(error)=>{
          console.log(error);
          if(this.load){ this.load.dismiss(); this.load = null; }
        });

      });
  }

  itemTapped(event, item, name) {
    // That's right, we're pushing to ourselves!
    this.events.publish('competition:selected', item, name);
      this.navCtrl.push(Home, {
        item: item
      });
  }
}
