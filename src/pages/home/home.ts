import { Component } from '@angular/core';
import { CompeticaoService } from '../../providers/competicao-service';
import { NavController, Events, ModalController, NavParams, LoadingController } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { ModalJogadoresSuspensos } from '../../components/modals/simplepage/ModalJogadoresSuspensos';
import { ModalJogadoresPendurados } from '../../components/modals/simplepage/ModalJogadoresPendurados';
import { ModalNotifications } from '../../components/modals/ModalNotifications';
import { Resultado } from '../resultado/resultado';
import { TabelaJogos } from '../tabelajogos/tabelajogos';
import 'rxjs/add/operator/finally';

import { DatabaseProvider } from '../../providers/database';

@Component({
  selector: 'page-page1',
  templateUrl: 'home.html',
  providers: [CompeticaoService]
})

export class Home {

   appName: string;
   partidas: Array<any>;
   idCompeticao: Number;
   competicaoList: Array<any>;
   categorias: Array<any>;
   loading: any;
   count_notif: any;

  constructor(
    public navCtrl: NavController,
    public modal: ModalController,
    public navParams: NavParams,
    public competicaoService: CompeticaoService,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public events: Events,
    private db: DatabaseProvider
  ){
    events.publish('clear:selected');
    this.appName = 'Gol de Placa - MA';
    this.loading = loadingCtrl.create({
      content: '',
      spinner: 'dots'
    });
    this.loading.present();
  }
  
  ionViewWillEnter(){
    this.countNotificationNotRead();
    this.competicaoService.findOne(this.navParams.get('item')).subscribe(data=>{
      if(data){
        this.competicaoService.findCategoria(this.navParams.get('item')).subscribe((cat)=>{
          this.categorias = cat;
        });
        this.idCompeticao = this.navParams.get('item');
        this.loadPartidas(this.idCompeticao);
      }
    });
  }

  loadPartidas(pk){
    /*let load = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    load.present();*/
    this.competicaoService.findPartida(pk).finally(()=>{
      this.loading.dismiss();
    }).subscribe((data)=>{
      console.table({'partidas':data});
      this.partidas = data;
    },(err)=>{
      console.log("ERRO LOAD PARTIDAS");
    });
  }
  openDetalhesJogo(id){
    this.events.publish('detalhesjogos:selected',id);
  }
  openMenu(){
    this.menuCtrl.open();
  }
  loadCompeticoes(){
    this.competicaoService.findAll()
    .subscribe(data=>{
      console.table({'partidas':data});
      this.competicaoList = data;
    });
  }
  jogadoresSuspensos(){
    let loading = this.loadingCtrl.create({content:'', spinner: 'dots'});
    loading.present();
    this.competicaoService.findJogadoresSuspensos(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalJogadoresSuspensos,{
          'jogadores': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  jogadoresPendurados(){
    let loading = this.loadingCtrl.create({content:'', spinner:'dots'});
    loading.present();
    this.competicaoService.findJogadoresPendurados(this.idCompeticao)
    .finally(()=>{
      loading.dismiss();
    })
    .subscribe((then)=>{
      this.modal.create(ModalJogadoresPendurados,{
          'jogadores': then
      }).present();
    },(error)=>{
      console.log(error);
    });
  }
  classificacao(categoria){
      let competicao = this.idCompeticao;
      let loading = this.loadingCtrl.create({content:'', spinner:'dots'});
      loading.present();
      this.competicaoService.findClassificacao(competicao,categoria)
      .finally(()=>{
        loading.dismiss();
      })
      .subscribe((data)=>{
        this.navCtrl.push(Resultado, {
          data: data,
          competicao: competicao
        });
      });
  }
  jogos(categoria){
    let competicao = this.idCompeticao;
    this.navCtrl.push(TabelaJogos, {
      competicao: competicao,
      categoria: categoria
    });
  }
  async countNotificationNotRead(){
    await this.db.getCountNotRead().then((data)=>{
      this.count_notif = data;
    });;
  }
  getNotifications(){
    this.modal.create(ModalNotifications).present();
  }
}
