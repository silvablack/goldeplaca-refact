import { Component, ViewChild } from '@angular/core';
import { NavController, Events,NavParams, MenuController, ModalController,ViewController, Slides } from 'ionic-angular';
import { ModalClassificacao } from '../../components/modals/classificacao/ModalClassificacao';
import { ModalArtilheiroDetalhes } from '../../components/modals/ModalArtilheiroDetalhes';
import { ModalMelhorDisciplinaDetalhes } from '../../components/modals/ModalMelhorDisciplinaDetalhes';
import { ModalAtaquePositivoDetalhes } from '../../components/modals/ModalAtaquePositivoDetalhes';
import { ModalMelhorDefesaDetalhes } from '../../components/modals/ModalMelhorDefesaDetalhes';
import { ModalGolsContraDetalhes } from '../../components/modals/ModalGolsContraDetalhes';
import { ModalGoleiroArtilheiroDetalhes } from '../../components/modals/ModalGoleiroArtilheiroDetalhes';
import { Home } from '../home/home';
import { CompeticaoService } from '../../providers/competicao-service';
/**
 * Generated class for the ClassificacaoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-resultado',
  templateUrl: 'resultado.html',
})
export class Resultado{
  @ViewChild(Slides) slides: Slides;
  public data: any;
  public classificacao: Array<any>;
  public equipes_ch: Array<any>;
  public artilheiros: Array<any>;
  public melhorDisciplina: Array<any>;
  public ataquePositivo: Array<any>;
  public melhorDefesa: Array<any>;
  public golsContra: Array<any>;
  public goleiroArtilheiro: Array<any>;
  public competicao;
  constructor(public navCtrl: NavController,
              public menuCtrl: MenuController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public viewCtrl: ViewController,
            public events: Events,
            public competicaoService: CompeticaoService
          ){
            this.competicao = navParams.get('competicao');
  menuCtrl.close();
  }

  /*OPEN MODALS FOR Action Click*/

  openClassificacaoDetalhes(index){
    this.modalCtrl.create(ModalClassificacao,{id: index}).present();
  }

  openArtilheirosDetalhes(index){
    console.table({index:index, data: this.artilheiros[index]});
    this.modalCtrl.create(ModalArtilheiroDetalhes,{artilheiro: this.artilheiros[index]}).present();

  }
  openJogadorDetalhes(n,c,i){
    this.events.publish('jogadordetalhes:selected',n,c,i);
  }

  openMelhorDisciplinaDetalhes(index){
    this.modalCtrl.create(ModalMelhorDisciplinaDetalhes,{equipe: this.melhorDisciplina[index]}).present();
  }

  openAtaquePositivoDetalhes(index){
    this.modalCtrl.create(ModalAtaquePositivoDetalhes,{equipe: this.ataquePositivo[index]}).present();
  }

  openMelhorDefesaDetalhes(index){
    this.modalCtrl.create(ModalMelhorDefesaDetalhes,{equipe: this.melhorDefesa[index]}).present();
  }

  openGolsContraDetalhes(index){
    this.modalCtrl.create(ModalGolsContraDetalhes,{jogador: this.golsContra[index]}).present();
  }

  openGoleiroArtilheiroDetalhes(index){
    this.modalCtrl.create(ModalGoleiroArtilheiroDetalhes,{jogador: this.goleiroArtilheiro[index]}).present();
  }


  transformtoarray(data): Array<any>{
    //TRANSFORM IN DATA ARRAY
    console.log('transformtoarray');
    let keys = Object.keys(data);
    console.log(keys);
    let newdata: Array<any> = [];
    for(let key in keys){
      newdata.push({key: keys[key], value: data[keys[key]]});
    }
    return newdata;
  }

  ionViewDidEnter() {
    this.data = this.navParams.get('data');
    this.classificacao = this.data['classificacao'];
    this.artilheiros = this.data['artilheiros'];
    this.melhorDisciplina = this.data['classificacao_disciplinar'];
    this.ataquePositivo = this.data['ataquemais'];
    this.melhorDefesa= this.data['melhordefesa'];
    this.golsContra = this.data['gols_contra'];
    this.goleiroArtilheiro = this.data['artilheiros_goleiros'];
    this.equipes_ch = this.transformtoarray(this.data['equipes_chaveada']);
    console.table({'data-competicao':this.data});
  }

  //RESOLVENDO BUG QUANDO EXISTE APENAS UMA CATEGORIA E NAO DA PRA USAR A ACAO ionChange
  ionViewWillLeave(){
    this.competicaoService.findOne(this.competicao)
    .subscribe((competicao)=>{
      this.events.publish('competition:selected', competicao[0].id,competicao[0].nome_competicao);
        this.navCtrl.setRoot(Home, {
          item: competicao[0].id
        });
    });

  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
