
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {

    constructor(private db: SQLite){
    }

    public getDb(){
        return this.db.create({
          name: 'goldeplaca.db',
          location: 'default'
        })
      }

    public createDatabase(){
        return this.getDb().then((db: SQLiteObject) => {
          this.createTableDb(db);
        }).catch((err) => {
          console.log(err);
        });
      }
    
    private createTableDb(db: SQLiteObject){
        db.sqlBatch([
          ['CREATE TABLE IF NOT EXISTS notifications (id integer primary key AUTOINCREMENT NOT NULL, notification_id TEXT, title TEXT, body TEXT, readed interger, created_at DATE)'],
        ]).then(()=>console.log('tabela criada'))
        .catch((err)=>console.log(err));
    }
    
    public insertItem(notification, open = false){
        return this.getDb().then((db: SQLiteObject)=>{
          let sql = 'insert into notifications (title, body, notification_id, readed, created_at) values (?, ?, ?, ?, ?)';
          let data = [notification.title, notification.body, notification.notificationID, open ? 1 : 0, Date.now()];
     
            return db.executeSql(sql, data)
              .catch((e) => console.log(e));
        })
    }

    public getAll(){
        return this.getDb().then((db: SQLiteObject)=>{
            let sql = 'select * from notifications ORDER BY readed';
              return  db.executeSql(sql, []).then((data)=>{
                  if(data.rows.length > 0){
                    let resultArray: any[] = [];
                    for(var i = 0; i < data.rows.length; i++ ){
                      console.table({funcao: 'database.ts', data: data});
                      var r = data.rows.item(i);
                      resultArray.push(r);
                    }
                    
                    return resultArray;
                  }else{
                    return [];
                  }
                   
                }).catch((e) => console.log(e));
        }).catch((err)=>console.log(err));
    }

    updateRead(id, params){
      return this.getDb().then((db: SQLiteObject)=>{
        let sql = 'update notifications set readed = ? where id = ?';
        return db.executeSql(sql, [ params, id ]).catch((e)=>console.log(e));
      }).catch((e)=>console.log(e));
    }

    // this function filter by name notification ID OneSignal
    setReadedNotificationOpened(id){
      return this.getDb().then((db: SQLiteObject)=>{
        let sql = 'update notifications set readed = ? where notification_id = ?';
        return db.executeSql(sql, [ 1, id ]).catch((e)=>console.log(e));
      }).catch((e)=>console.log(e));
    }

    getCountNotRead(){
      return this.getDb().then((db: SQLiteObject)=>{
        let sql = 'select count(*) as total from notifications where readed = 0';
        return db.executeSql(sql, []).then((data)=>{
          console.table({func: 'getCount', not: data});
          return data.rows.item(0).total;
        }).catch((e)=>console.log(e));
      }).catch((e)=>console.log(e));
    }
}